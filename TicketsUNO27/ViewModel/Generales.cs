﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
        public class CustoErrors
        {
            public string Name { get; set; }
            public string Message { get; set; }
            public CustomErrorType Type { get; set; }
        }

        public enum CustomErrorType
        {
            Simple = 0,
            Nofound = 404,
            badRequest = 500
        }

    // Estratos
    public static class _Estratos
    {
        public static Guid Estrato1 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato2 { get; set; } = new Guid("3901d691-2f04-4260-a613-4bea542bf053");
        public static Guid Estrato3 { get; set; } = new Guid("303396dd-26a5-4ed5-8de6-c71f2cb27b8a");
        public static Guid Estrato4 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato5 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato6 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");

 
    }


    // estados de las gestiones
    public static class _EstadosGestion
    {
        // venta ok
        public static Guid Temporal { get; set; } = new Guid("d93b2c82-ea74-4d49-8a3d-a1c4ad08a1e5");

        public static Guid VentaCantada { get; set; } = new Guid("e820e674-e7cd-4087-bcd2-e68a0281511d");
        public static Guid VentaDigitada { get; set; } = new Guid("785e5148-50f7-4c43-a7f3-23a8c9c68f0b");
        public static Guid VentaSoportada { get; set; } = new Guid("1046e22e-abef-4ea2-beda-8cae01620534");
        public static Guid AuditadaOk { get; set; } = new Guid("80245538-98ea-4c52-86d3-8468aa798e60");
        public static Guid VentaLegalizada { get; set; } = new Guid("7ada6687-c83a-4780-9e8c-4eeb6116dd82");

        public static Guid EnGestion { get; set; } = new Guid("46E6DB07-510A-4715-81AC-40CC4F7733BC");
        public static Guid NoVenta { get; set; } = new Guid("9B6B7E0B-1256-4842-9F46-165555C202E8");

        // caida
        public static Guid CaidaInstalacion { get; set; } = new Guid("fbe84570-8692-47f0-b44c-3f239577c1a3");
        public static Guid CaidaValidacion { get; set; } = new Guid("1feb41f9-593c-451f-9d0d-418b260eb325");
        public static Guid CaidaLegalizacion { get; set; } = new Guid("2d83ebe0-4b2c-4092-9ddf-692e7c6f2509");
        public static Guid CaidaDigitacion { get; set; } = new Guid("ef997b1a-f56d-497e-82fd-f5c981a3ba39");
        public static Guid RechazadaAgente { get; set; } = new Guid("20ff2f35-5b50-4873-848d-c4751684d720");
        public static Guid CaidaTextoLegal  { get; set; } = new Guid("38d3c94f-9474-47c4-b208-f7629ac228dd");
        public static Guid CaidaDigitacionAgente  { get; set; } = new Guid("52f4529d-21d5-45f1-8cc0-54ead0ea6eea");

        public static List<Guid> LEstadosCaida = new List<Guid>() {
            CaidaInstalacion ,
            CaidaValidacion,
            CaidaLegalizacion,
            CaidaDigitacion,
            RechazadaAgente,
            CaidaTextoLegal,
            CaidaDigitacionAgente
        };

        public static List<Guid> LEstadosRecuperarAgente = new List<Guid>
        {
            CaidaTextoLegal,
            CaidaDigitacionAgente

        };

        public static List<Guid> LEstadosAprobados = new List<Guid>()
        {
            VentaDigitada,
            AuditadaOk,
            VentaSoportada,
            VentaLegalizada
        };

    }

    /*
     * ROLES DE LA APLICACIÓN
     */

    public static class _Roles
    {
        public static Guid Asesor { get; set; } = new Guid("27c3d61f-aefe-40ce-8eaa-3292117fa4b6");
        public static Guid Validador { get; set; } = new Guid("5f0614f8-65e8-43f9-96a1-1bebcbaf549d");
        public static Guid Administrador { get; set; } = new Guid("520469d8-724c-4cb6-96c2-45001209a39b");
        
    }

    /*
     * LISTA DE CONFIGURACIONES 
     */
    public static class _Configuraciones
    {

        //Configuraciones de servicios.
        public static Guid Megasdevelocidad { get; set; } = new Guid("1bdf11d8-4105-4e7d-8143-2f42ef87e1b0");
        public static Guid PlanesdeTv { get; set; } = new Guid("5b2d93de-9891-4d8b-a737-e4ab3b531221");
        public static Guid planesMinutos { get; set; } = new Guid("3bada147-c553-4c7b-affa-ae2e134789cb");


        public static Guid Permanencia { get; set; } = new Guid("18601018-fe6c-4c8c-87d4-ddd2f0c52705");
        public static Guid Entregadecorrespondencia { get; set; } = new Guid("83614486-50b8-454d-9ee3-de684c848e4b");
        public static Guid CobrodeInstalacion { get; set; } = new Guid("f82a1ac6-773b-4be3-9663-e4feed60b2cb");
        public static Guid Tomasadicionalesdetv { get; set; } = new Guid("217a19e8-0e1e-403d-b561-1b646748847b");
        public static Guid Multiplay { get; set; } = new Guid("aad7e420-37a6-4a2c-b7b6-736349e35b91");
        public static Guid CuentaMatriz { get; set; } = new Guid("f7b00f24-508d-4966-9506-7e0723ce0c7a");
        public static Guid Tipodesolicitud { get; set; } = new Guid("6890508e-a216-4d15-afee-ba8e5f4e794f");
        public static Guid TipodeRed { get; set; } = new Guid("33514a02-c775-4bfc-8f01-bef07270136b");
        public static Guid Tipodecliente { get; set; } = new Guid("889e0422-8dcb-45de-b1ce-32a775b9d88a");
        public static Guid TipoDocumento { get; set; } = new Guid("c0fdbcc6-d4ec-4500-8425-366a5af61748");
        public static Guid Estratos { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Politica { get; set; } = new Guid("0793b9d1-03c6-4794-991c-826605cc7390");
        public static Guid campanas { get; set; } = new Guid("919d0c4c-2e3e-4986-9115-9828a2ab07c5");
        public static Guid RechazosCalidad { get; set; } = new Guid("89b2b7d4-ad9b-44af-83eb-b0fe36d30ebe");
        public static Guid RechazosDigitacion { get; set; } = new Guid("4e08646b-9eaf-466a-9b89-25e5867684ac");
        public static Guid RechazosInstalacion { get; set; } = new Guid("950bff05-22d2-4454-b554-abfbad56a349");
        public static Guid RechazosLegalizacion { get; set; } = new Guid("1b841d01-8fb4-41eb-b229-d1dfb012c0e3");

        //INSTALCIÓN.
        public static Guid Instalacion_EstadoInicial { get; set; } = new Guid("e2ca7ca4-a313-46a2-86ee-5297c290cdbc");
        public static Guid instalacion_EstadoFinal { get; set; } = new Guid("331cc1a5-1e5b-485e-9089-5e51ce785a06");
        public static Guid Instalacion_RazonAgendamiento { get; set; } = new Guid("da1c19b3-fa50-42f4-ba66-eff08be75237");

        // DATOS PARA LA TIPIFICACION DE INBOUND


        // DATOS PARA NUEVO MUDULO DE ADMINSITRACION DE PRODUCTOS.
        public static Guid PRODUCTOS_TipoDeVenta { get; set; } = new Guid("34ed6722-e5b8-42af-9256-0d299f5e14f8");






        /*========================================================================================================*/
        /*===========|      CONFIGURACIONES PARA EL AYUDAVENTAS         |*/
        /*========================================================================================================*/



        //Configuraciones de servicios.
        public static Guid Campana_tiposCiudades { get; set; } = new Guid("9f6d6cb7-1ec0-4955-80ba-ec22a61f144c");
        public static Guid Campana_TipoProducto { get; set; } = new Guid("ca5db5bd-cd47-4722-9dfd-d5da953af7e8");
        public static Guid Campana_TipoOferta { get; set; } = new Guid("77c624b5-1e16-46e9-b751-3b22d6c0d2b3");

        public static Guid Campana_DependePetar { get; set; } = new Guid("814C704B-7616-47AE-AA53-86F8E45AF7D8"); // se usa para saber si depende de ptar


        // Datos de la campña

        public static Guid ConfiguracionCampana_tipo { get; set; } = new Guid("b8080317-f35c-47e8-a519-885ee52e4139");




    }

    /*
     * LISTA DE GESTIONES 
     */

    public static class _Tipificaciones
    {

        //  VENTAS
        public static int VentaFijo1 { get; set; } = 25;
        public static int VentaFijo2 { get; set; } = 67;
        public static int VentaFijo3 { get; set; } = 108;
        public static int VentaMovil1 { get; set; } = 26;
        public static int VentaMovil2 { get; set; } = 68;
        public static int VentaMovil3 { get; set; } = 109;
        public static int VentaMultiplay1 { get; set; } = 27;
        public static int VentaMultiplay2 { get; set; } = 69;
        public static int VentaMultiplay3 { get; set; } = 110;

        //PREVENTAS PADRES
        public static int Preventa1 { get; set; } = 170;
        public static int Preventa2 { get; set; } = 172;
        public static int Preventa3 { get; set; } = 174;
        public static int Preventa6 { get; set; } = 304;
        
        //PREVENTAS
        public static int PreventaHPP1 { get; set; } = 171;
        public static int PreventaHPP2 { get; set; } = 173;
        public static int PreventaHPP3 { get; set; } = 175;
        public static int PreventaHPP6 { get; set; } = 305;

        public static int ACW { get; set; } = 13;

        public static List<int> LPreventas { get; set; } = new List<int>()
        {
            PreventaHPP1,
            PreventaHPP2,
            PreventaHPP3,
            PreventaHPP6
        };
    }

    public static class _EstadoCliente
    {
        public static Guid Activo { get; set; } = new Guid("1eb77c67-16ff-4247-b875-001f055f4854");
        public static Guid Inactivo { get; set; } = new Guid("afc34a8d-4885-422a-b13a-b2b1079a5c6e");
    }
}