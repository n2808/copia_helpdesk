﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class CustomError
    {
        public string Name { get; set; }
        public string Message { get; set; }

        public enum CustomErrorType
        {
            Simple = 0,
            Nofound = 404,
            badRequest = 500
        }
    }
}