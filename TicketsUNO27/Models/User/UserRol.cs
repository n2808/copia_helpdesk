﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.User
{
	public class UserRol : EntityWithIntId
    {
        [DisplayName("Perfil")]
		public int RolId { get; set; }

        [DisplayName("Usuario")]
        public int UserId { get; set; }

		[ForeignKey("RolId")]
		public Rol Rol  { get; set; }

		[ForeignKey("UserId")]
		public User User { get; set; }
	}
}