﻿
using AppRecordatorio.Models.Campanas;
using Core.Models.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.User
{
	public class User : EntityWithIntId
    {
        [Required]
        public string Documento { get; set; }

        [Required]
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public bool Estado { get; set; }

        [Required]
        public string Clave { get; set; }   
        public List<UserRol> UserRol { get; set; }

        [Required]
        public string Login { get; set; }

        [DisplayName("Teléfono fijo")]
        public string TelefonoFijo { get; set; }
        public string Celular { get; set; }
        public string CodigoAcceso { get; set; }

        [DisplayName("Campaña")]

        [Required]
        public int CampanaId { get; set; }
        [ForeignKey("CampanaId")]

        public Campana Campana { get; set; }

        public string NombreCampana { get; set; }
    }
}