﻿namespace Core.Models.Security
{
    public class Accounts
	{
		public int? Id { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
		public string[] Roles { get; set; }
	}
}