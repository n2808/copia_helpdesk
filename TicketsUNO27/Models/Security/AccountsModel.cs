﻿
using AppRecordatorio.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Models.Security
{
	public class AccountsModel
	{
        private Contexto db = new Contexto();

        private List<Accounts> ListAccounts = new List<Accounts>();


		public Accounts find(int? Id = null)
		{
			var Users = db.Usuario.Find(Id);
			string[] Roles = db.UsuariosRoles.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();
			Accounts UserAccount = new Accounts
			{
				UserName = Users.Nombres + " " + Users.Apellidos,
				Roles = Roles,
				Id = Users.Id
			};
			return UserAccount;
		}


		public Accounts Login(string Documento, string Password) {
			var Users = db.Usuario.Where(c => c.Documento == Documento && c.Clave == Password && c.Estado == true).FirstOrDefault();
			if (Users != null) {
				string[] Roles = db.UsuariosRoles.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();
                

                Accounts UserAccount = new Accounts
				{
					UserName = Users.Nombres + " " + Users.Apellidos
					,
					Roles = Roles
					,
					Id = Users.Id
				};
				return UserAccount;
			}
			{
				return null; 
			}
			
		}
	}
} 