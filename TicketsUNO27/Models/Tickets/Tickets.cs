﻿using AppRecordatorio.Models.Areas;
using AppRecordatorio.Models.Estados;
using AppRecordatorio.Models.TipoCasos;
using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppRecordatorio.Models.TicketsCasos
{
    public class Tickets : EntityWithIntId
    {
        public int IdUsuario { get; set; } 
        public int IdArea { get; set; }
        public int IdTipoCaso { get; set; }

        [Display(Name = "Fecha Finalizacion")]
        public DateTime? FechaFinalizacion { get; set; }
        public int IdEstado { get; set; }
        public int? IdTipoAtencion { get; set; }
        public string Incidente { get; set; }
        public string Respuesta { get; set; }
        public string DetalleDemoraGestion { get; set; }

        [Display(Name = "Asignado")]
        public int? IdAsignado { get; set; }       

        [Display(Name = "Asignado Por")]
        public int? IdAsignadoPor { get; set; }
        public string Modalidad { get; set; }
        public string IpUsuario { get; set; }

        [Display(Name = "Notas Ticket")]
        public string Notas { get; set; }

        public int? CreadoPor { get; set; }
        public int? ActualizadoPor { get; set; }

        #region

        [ForeignKey("IdUsuario")]
        public User User { get; set; }

        [ForeignKey("IdAsignadoPor")]
        public User AsignadoPor { get; set; }

        [ForeignKey("IdAsignado")]
        public User Users { get; set; }

        [ForeignKey("IdArea")]
        public Area Area { get; set; }

        [ForeignKey("IdEstado")]
        public Estado Estados { get; set; }

        [ForeignKey("IdTipoAtencion")]
        public Estado TipoAtencion { get; set; }

        [ForeignKey("IdTipoCaso")]
        public TipoCaso TipoCaso { get; set; }
        #endregion
    }
}