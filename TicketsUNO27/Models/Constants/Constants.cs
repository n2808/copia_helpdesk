﻿namespace TicketsUNO27.Models.Constants
{
    public class Constants
    {
        public const string PrioridadAlta  = "0 - 60 Minutos";
        public const string PrioridadMedia = "1 - 24 Horas";
        public const string PrioridadBaja  = "3 - 72 Horas";

        public const string Alto  = "ALTO";
        public const string Media = "MEDIA";
        public const string Baja  = "BAJA";


        public const int RolAsesor      = 4;
        public const int AtencionRemota = 7;
        public const int AtencionSitio  = 8;
    }
}