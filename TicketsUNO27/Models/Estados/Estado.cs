﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace AppRecordatorio.Models.Estados
{
    public class Estado : EntityWithIntId
    {
        [Display(Name = "Estado")]
        public string Nombre { get; set; }
        public bool EsFinal { get; set; }

    }
}