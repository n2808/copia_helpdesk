﻿using AppRecordatorio.Models.TicketsCasos;
using Core.Models.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketsUNO27.Models.Archivos
{
    public class Archivos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Archivo { get; set; }

        public string Nombre { get; set; }

        public string ExtensionArchivo { get; set; }

        public int? TicketId { get; set; }

        [ForeignKey("TicketId")]
        public Tickets Tickets { get; set; }

        public int? IdUsuarioTicket { get; set; }

        [Display(Name = "UsuarioCrea")]
        [ForeignKey("IdUsuarioTicket")]
        public User User { get; set; }

        public int? IdRespuestaPor { get; set; }
        [ForeignKey("IdRespuestaPor")]
        public User ImgSubidaPor { get; set; }

    }
}