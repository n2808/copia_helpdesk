﻿namespace AppRecordatorio.Models
{
    using AppRecordatorio.Models.Areas;
    using AppRecordatorio.Models.Campanas;
    using AppRecordatorio.Models.Estados;
    using AppRecordatorio.Models.TicketsCasos;
    using AppRecordatorio.Models.TipoCasos;
    using Core.Models.User;
    using System.Data.Entity;
    using TicketsUNO27.Models.Archivos;
    using TicketsUNO27.Models.Historial;

    public class Contexto : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'Contexto' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'tmkhogares.Models.Contexto' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'Contexto'  en el archivo de configuración de la aplicación.
        public Contexto()
            : base("name=Contexto")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        #region USUARIOS
        public DbSet<User> Usuario { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<UserRol> UsuariosRoles { get; set; }
        #endregion


        #region TABLAS BASE
        public DbSet<Area> Areas { get; set; }
        public DbSet<Campana> Campanas { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Tickets> Tickets { get; set; }
        public DbSet<TipoCaso> TipoCasos { get; set; }
        public DbSet<Archivos> Archivos { get; set; }
        #endregion

        #region HISTORIAL

        public DbSet<HistorialTickets> HistorialTickets { get; set; }
        #endregion
    }

}