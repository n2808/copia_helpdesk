﻿using AppRecordatorio.Models.Areas;
using Core.Models.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppRecordatorio.Models.TipoCasos
{
    public class TipoCaso : EntityWithIntId
    {
        public int IdArea { get; set; }
        [ForeignKey("IdArea")]
        public Area Area { get; set; }
        [Display(Name = "Tipo de Caso")]
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public string Prioridad { get; set; }

    }
}