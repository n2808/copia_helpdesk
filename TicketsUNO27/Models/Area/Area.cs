﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppRecordatorio.Models.Areas
{
    public class Area : EntityWithIntId
    {
        [Display(Name = "Area")]
        public string Nombre { get; set; }

        public bool Estado { get; set; }

    }
}