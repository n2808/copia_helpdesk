﻿using Core.Models.Common;
using System.ComponentModel;

namespace AppRecordatorio.Models.Campanas
{
    public class Campana : EntityWithIntId
    {
        [DisplayName("Campaña")]
        public string Nombre { get; set; }
        public string ClienteCampana { get; }
        public bool Estado { get; set; }
    }
}