﻿using AppRecordatorio.Models.Estados;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TicketsUNO27.Models.Historial
{
    public class HistorialTickets
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int IdTicket { get; set; }

        public DateTime? FechaCambioEstado { get; set; }

        public int? IdEstado { get; set; }
        [ForeignKey("IdEstado")]
        public Estado Estados { get; set; }

        public string Respuesta { get; set; }

        public int? AsignadoPor { get; set; }
        [Display(Name = "Asignador Por")]
        [ForeignKey("AsignadoPor")]
        public User TicketAsignadoPor { get; set; }

        public int? AsignadoA { get; set; }
        [Display(Name = "Asignado A")]
        [ForeignKey("AsignadoA")]
        public User TicketAsignadoA { get; set; }

        [Display(Name = "Notas Ticket")]
        public string NotasTicket { get; set; }
    }
}