﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Secreafuncionalidadparaevaluarlademoradeunticketengestionar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "IdTipoAtencion", c => c.Int());
            AddColumn("dbo.Tickets", "DetalleDemoraGestion", c => c.String());
            CreateIndex("dbo.Tickets", "IdTipoAtencion");
            AddForeignKey("dbo.Tickets", "IdTipoAtencion", "dbo.Estadoes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "IdTipoAtencion", "dbo.Estadoes");
            DropIndex("dbo.Tickets", new[] { "IdTipoAtencion" });
            DropColumn("dbo.Tickets", "DetalleDemoraGestion");
            DropColumn("dbo.Tickets", "IdTipoAtencion");
        }
    }
}
