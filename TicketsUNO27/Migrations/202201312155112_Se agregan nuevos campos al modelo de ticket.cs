﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seagregannuevoscamposalmodelodeticket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "CreadoPor", c => c.Int());
            AddColumn("dbo.Tickets", "ActualizadoPor", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "ActualizadoPor");
            DropColumn("dbo.Tickets", "CreadoPor");
        }
    }
}
