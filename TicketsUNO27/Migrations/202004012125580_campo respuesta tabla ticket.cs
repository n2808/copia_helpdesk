﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camporespuestatablaticket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Respuesta", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "Respuesta");
        }
    }
}
