﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiosentablas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "IdEstado", c => c.Int(nullable: false));
            AlterColumn("dbo.Campanas", "Estado", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Tickets", "IdEstado");
            AddForeignKey("dbo.Tickets", "IdEstado", "dbo.Estadoes", "Id", cascadeDelete: true);
            DropColumn("dbo.Tickets", "Estado");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "Estado", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Tickets", "IdEstado", "dbo.Estadoes");
            DropIndex("dbo.Tickets", new[] { "IdEstado" });
            AlterColumn("dbo.Campanas", "Estado", c => c.String());
            DropColumn("dbo.Tickets", "IdEstado");
        }
    }
}
