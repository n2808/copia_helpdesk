﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camponombrecampañatablausuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "NombreCampana", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "NombreCampana");
        }
    }
}
