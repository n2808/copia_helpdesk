﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregacolumnaasignadoatablaticketsopcional : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "IdAsignado", c => c.Int(nullable: true));
            CreateIndex("dbo.Tickets", "IdAsignado");
            AddForeignKey("dbo.Tickets", "IdAsignado", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "IdAsignado", "dbo.Users");
            DropIndex("dbo.Tickets", new[] { "IdAsignado" });
            DropColumn("dbo.Tickets", "IdAsignado");
        }
    }
}
