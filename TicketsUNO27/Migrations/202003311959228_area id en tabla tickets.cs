﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class areaidentablatickets : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TipoCasoes", "IdArea", "dbo.Areas");
            DropIndex("dbo.TipoCasoes", new[] { "IdArea" });
            AddColumn("dbo.Tickets", "IdArea", c => c.Int(nullable: false));
            CreateIndex("dbo.Tickets", "IdArea");
            AddForeignKey("dbo.Tickets", "IdArea", "dbo.Areas", "Id", cascadeDelete: true);
            DropColumn("dbo.TipoCasoes", "IdArea");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TipoCasoes", "IdArea", c => c.Int(nullable: false));
            DropForeignKey("dbo.Tickets", "IdArea", "dbo.Areas");
            DropIndex("dbo.Tickets", new[] { "IdArea" });
            DropColumn("dbo.Tickets", "IdArea");
            CreateIndex("dbo.TipoCasoes", "IdArea");
            AddForeignKey("dbo.TipoCasoes", "IdArea", "dbo.Areas", "Id", cascadeDelete: true);
        }
    }
}
