﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambioscambiostablatipocaso : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Incidente", c => c.String());
            AddColumn("dbo.Users", "Celular", c => c.String());
            DropColumn("dbo.Users", "Ceular");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Ceular", c => c.String());
            DropColumn("dbo.Users", "Celular");
            DropColumn("dbo.Tickets", "Incidente");
        }
    }
}
