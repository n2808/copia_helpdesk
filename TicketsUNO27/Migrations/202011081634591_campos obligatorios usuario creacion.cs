﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camposobligatoriosusuariocreacion : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Documento", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Nombres", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Clave", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Login", c => c.String());
            AlterColumn("dbo.Users", "Clave", c => c.String());
            AlterColumn("dbo.Users", "Nombres", c => c.String());
            AlterColumn("dbo.Users", "Documento", c => c.String());
        }
    }
}
