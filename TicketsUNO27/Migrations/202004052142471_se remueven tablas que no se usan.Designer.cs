﻿// <auto-generated />
namespace TicketsUNO27.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class seremueventablasquenoseusan : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(seremueventablasquenoseusan));
        
        string IMigrationMetadata.Id
        {
            get { return "202004052142471_se remueven tablas que no se usan"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
