﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregacampomodalidadtablatickets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Modalidad", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "Modalidad");
        }
    }
}
