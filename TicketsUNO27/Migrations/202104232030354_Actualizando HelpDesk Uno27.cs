﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandoHelpDeskUno27 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TipoCasoes", "TiempoResolucion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TipoCasoes", "TiempoResolucion", c => c.Int(nullable: false));
        }
    }
}
