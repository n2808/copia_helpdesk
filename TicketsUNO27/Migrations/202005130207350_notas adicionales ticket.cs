﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class notasadicionalesticket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Notas", c => c.String());
            DropColumn("dbo.HistorialTickets", "Incidencia");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HistorialTickets", "Incidencia", c => c.String());
            DropColumn("dbo.Tickets", "Notas");
        }
    }
}
