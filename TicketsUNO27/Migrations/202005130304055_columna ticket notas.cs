﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class columnaticketnotas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistorialTickets", "NotasTicket", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HistorialTickets", "NotasTicket");
        }
    }
}
