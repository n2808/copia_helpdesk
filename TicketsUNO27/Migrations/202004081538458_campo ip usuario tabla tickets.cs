﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class campoipusuariotablatickets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "IpUsuario", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "IpUsuario");
        }
    }
}
