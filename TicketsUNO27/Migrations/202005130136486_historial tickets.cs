﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class historialtickets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HistorialTickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdTicket = c.Int(nullable: false),
                        FechaCambioEstado = c.DateTime(),
                        IdEstado = c.Int(),
                        Incidencia = c.String(),
                        Respuesta = c.String(),
                        AsignadoPor = c.Int(),
                        AsignadoA = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estadoes", t => t.IdEstado)
                .ForeignKey("dbo.Users", t => t.AsignadoA)
                .ForeignKey("dbo.Users", t => t.AsignadoPor)
                .Index(t => t.IdEstado)
                .Index(t => t.AsignadoPor)
                .Index(t => t.AsignadoA);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistorialTickets", "AsignadoPor", "dbo.Users");
            DropForeignKey("dbo.HistorialTickets", "AsignadoA", "dbo.Users");
            DropForeignKey("dbo.HistorialTickets", "IdEstado", "dbo.Estadoes");
            DropIndex("dbo.HistorialTickets", new[] { "AsignadoA" });
            DropIndex("dbo.HistorialTickets", new[] { "AsignadoPor" });
            DropIndex("dbo.HistorialTickets", new[] { "IdEstado" });
            DropTable("dbo.HistorialTickets");
        }
    }
}
