﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregaareaidtipocaso : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TipoCasoes", "IdArea", c => c.Int(nullable: false));
            CreateIndex("dbo.TipoCasoes", "IdArea");
            AddForeignKey("dbo.TipoCasoes", "IdArea", "dbo.Areas", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TipoCasoes", "IdArea", "dbo.Areas");
            DropIndex("dbo.TipoCasoes", new[] { "IdArea" });
            DropColumn("dbo.TipoCasoes", "IdArea");
        }
    }
}
