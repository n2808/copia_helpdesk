﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregaprioridadentipocaso : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TipoCasoes", "Prioridad", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TipoCasoes", "Prioridad");
        }
    }
}
