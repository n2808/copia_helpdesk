﻿namespace TicketsUNO27.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregacampoenticketscreadopor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "IdAsignadoPor", c => c.Int());
            CreateIndex("dbo.Tickets", "IdAsignadoPor");
            AddForeignKey("dbo.Tickets", "IdAsignadoPor", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "IdAsignadoPor", "dbo.Users");
            DropIndex("dbo.Tickets", new[] { "IdAsignadoPor" });
            DropColumn("dbo.Tickets", "IdAsignadoPor");
        }
    }
}
