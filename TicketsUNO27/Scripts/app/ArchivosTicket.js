﻿function detalleImagen(id) {

    $("#detalleImagen").show();

    $('#imagenTicket').attr('src', 'data:image/jpeg;base64,' + id).show();
}


$("#ocultarImagen").click(function (event) {
    event.preventDefault();
    $("#detalleImagen").hide();
    return false;
});


function detallePdf(id) {
    $("#detallePdf").show();
    $('#pdfTicket').attr('src', 'data:application/pdf;base64,' + id).show();

}


$("#ocultarPdfDetalle").click(function (event) {
    event.preventDefault();
    $("#detallePdf").hide();
    return false;
});
