﻿$("#CargarUsuarios").click(function () {

    if (window.FormData == undefined)
        alert('Error: FormData is undefined');

    else {
        var fileUpload = $('#fileUsuarios').get(0);
        var files = fileUpload.files;
        var campana = $('#CampanaUsuario').val();

        if (campana == "0") {
            swal({
                position: 'top-right',
                type: 'error',
                title: 'Debe seleccionar una Campaña',
                showConfirmButton: false,
                timer: 2000
            });
        }

        if (files.length === 0) {
            swal({
                position: 'top-right',
                type: 'error',
                title: 'Por favor seleccione el archivo',
                showConfirmButton: false,
                timer: 2000
            });
        } else {
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);
            fileData.append('idCampana', campana);
            $.ajax({
                url: '/CargueUsuarios/CargarUsuarios',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                async: false,
                data: fileData,
                success: function (response) {
                    if (response.status === 201) {
                        swal({
                            position: 'top-right',
                            type: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    else if (response.status === 500) {
                        swal({
                            position: 'top-right',
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2500
                        });
                    }
                    else {
                        swal({
                            position: 'top-right',
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }



                }
            });
        }

    }



});