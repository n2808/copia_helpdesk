﻿$("body").on("click", "#CrearNuevoTicket", function () {

    if (ValidarCamposObligatorios()) {

        var files = $("#fileInput").get(0).files;
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            formData.append("fileInput", files[i]);
        }
        //formData.append('file', );
        formData.append("IdUsuario", $("#IdUsuario").val());
        formData.append("IdTipoCaso", $('#IdTipoCaso option:selected').val());
        formData.append("Incidente", $("#Incidente").val());
        formData.append("IdUsuario", $("#IdUsuario").val());
        formData.append("IdArea", $('#IdArea option:selected').val());
        formData.append("Modalidad", $('#Modalidad option:selected').val());


        $.ajax({
            type: "POST",
            url: '/Tickets/Create',
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                console.log(result);
                var _respuesta = result;

                if (_respuesta.type == "OK") {
                    swal({
                        position: 'top-right',
                        type: 'success',
                        title: _respuesta.message,
                        showConfirmButton: false,
                        timer: 2700
                    });
                    setTimeout(function () { location.href = "/Home/HomeAdmin" }, 2000);
                    $('#formCreacionTicket').trigger("reset");
                } 
                if (_respuesta.type == "ERROR") {
                    swal({
                        position: 'top-right',
                        type: 'error',
                        title: _respuesta.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }

            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                console.log(err);
                $('#formCreacionTicket').trigger("reset");
            }

        })
        //var data = {
        //    IdUsuario: $("#IdUsuario").val(),
        //    IdTipoCaso: $('#IdTipoCaso option:selected').val(),
        //    Incidente: $("#Incidente").val(),
        //    IdUsuario: $("#IdUsuario").val(),
        //    IdArea: $('#IdArea option:selected').val(),
        //    Modalidad: $('#Modalidad option:selected').val()
        //}




        //$.post("/Tickets/Create", data)
        //    .done((Result) => {
        //        console.log(Result)
        //        if (Result.type == "OK") {
        //            swal({
        //                position: 'top-right',
        //                type: 'success',
        //                title: Result.message,
        //                showConfirmButton: false,
        //                timer: 2700
        //            });
        //            setTimeout(function () { location.href = "/Home/HomeAdmin" }, 2000);
        //        }

        //        if (Result.type == "ERROR") {
        //            swal({
        //                position: 'top-right',
        //                type: 'error',
        //                title: Result.message,
        //                showConfirmButton: false,
        //                timer: 2000
        //            });
        //        }


        //    })
        //    .fail((data, status) => {
        //        _console("error interno ");
        //        _console(data);
        //        _console(status);

        //    })

        //$('#formCreacionTicket').trigger("reset");
    }

   
})


function ValidarCamposObligatorios() {

    var formValido = true;
    if ($('#IdArea option:selected').val() === "") {
        swal({
            position: 'top-right',
            type: 'error',
            title: "Por favor Seleccione el area",
            showConfirmButton: false,
            timer: 1500
        });
        return formValido = false;
    }
    if ($('#IdTipoCaso option:selected').val() === "") {
        swal({
            position: 'top-right',
            type: 'error',
            title: "Por favor Seleccione el tipo de caso",
            showConfirmButton: false,
            timer: 1500
        });
        return formValido = false;
    }
    if ($('#Modalidad option:selected').val() === "") {
        swal({
            position: 'top-right',
            type: 'error',
            title: "Por seleccione la modalidad",
            showConfirmButton: false,
            timer: 1500
        });
        return formValido = false;
    }
    if ($('#Incidente').val() === "") {
        swal({
            position: 'top-right',
            type: 'error',
            title: "Por favor describa la incidencia en el campo de texto",
            showConfirmButton: false,
            timer: 1500
        });
        return formValido = false;
    }
    return formValido = true;
}