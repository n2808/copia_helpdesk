﻿$('body').on('click', '#validar', function () {
    var estadoTicket = $('#IdEstado option:selected').val();
    var tipoAtencion = $('#IdTipoAtencion option:selected').val();
    var estadoDemoraGestion = $('#estadoDemora').val();
    var detalleDemoraGestion = $('#DetalleDemoraGestion').val();


    if (estadoDemoraGestion == 1 && (detalleDemoraGestion == "" || detalleDemoraGestion == undefined)) { $('.errorDetalleDemoraGestion').text('La nota por demora en la gestión es requerida').css('color', 'red'); return false }

    if (estadoTicket <= 0 || estadoTicket == "") { $('.errorEstadoTicket').text('El estado del ticket es requerido').css('color', 'red'); return false; }

    if (tipoAtencion <= 0 || tipoAtencion == "") { $('.errorTipoAtención').text('El tipo de atención es requerido').css('color', 'red'); return false }


    $('#Guardar').click();
  
});

$('body').on('change', '#IdEstado', function () { 
    $('.errorEstadoTicket').text(''); 
});

$('body').on('change', '#IdTipoAtencion', function () {
    $('.errorTipoAtención').text('');  
});

$('body').on('blur', '#DetalleDemoraGestion', function () {
    $('.errorDetalleDemoraGestion').text('');
});