﻿
$(document).ready(function () {

    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#example').DataTable({
        'ajax': {
            "url": "/Tickets/CargarTickets",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "Id", "autoWidth": true },
            { "data": "Id", "autoWidth": true },
            {
                "data": "Incidente",
                "className": 'CellWithComment',
                "render": function (data) {
                    return '<span class="textoIncidenciaTamano" title="' + data + '">' + data + '</span>';
                }
            },
            { "data": "Estados.Nombre", "autoWidth": true },
            { "data": "User.Documento", "autoWidth": true },
            { "data": "User.Nombres", "autoWidth": true },
            { "data": "User.Celular", "autoWidth": true },
            { "data": "User.TelefonoFijo", "autoWidth": true },
            { "data": "Modalidad", "autoWidth": true },
            { "data": "User.Campana.Nombre", "autoWidth": true },
            { "data": "TipoCaso.Prioridad", "autoWidth": true },
            { "data": "Users.Nombres", "autoWidth": true },
            { "data": "TipoCaso.Nombre", "autoWidth": true },
            {
                "data": "CreatedAt", "autoWidth": true, render: function (data, type, row) {
                    return moment(data).format("YYYY-MM-DD h:mm:ss");
                }
            },
            {
                "title": "Actions",
                "data": "Id",
                "searchable": false,
                "sortable": false,

                "render": function (data, type, full, meta) {
                    return '<a  href="/Tickets/Edit/' + data + '" class="ui red button">Detalle</a>';
                }
            }
        ],
        'columnDefs': [
            {
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                },
            }

        ],
        'select': {
            'style': 'multi'
        }
    });



    // Handle form submission event
    $('#btnTest').on('click', function (e) {
        var form = this;       

        var rows_selected = table.column(0).checkboxes.selected();

        if (rows_selected.length != 0) {
            var seleccionados = [];
            var idAsignado = $('#Usuarios option:selected').val();

            if (idAsignado == "" || idAsignado == undefined) {

                alert("Lo sentimos, el ticked debe ser asignado a un usuario.");

                return false;

            }

            $.each(rows_selected, function (index, rowId) {
                seleccionados.push(rowId)
            });

            var usuarioSeleccion = {

                Id: seleccionados,
                IdAsignado: idAsignado
            }

           

            $.ajax({
                type: "POST",
                url: '/Tickets/AsignarTicketUsuario',
                contentType: "application/json; charset=utf-8",
                processData: false,
                data: JSON.stringify(usuarioSeleccion),
                dataType: "json",
                success: function (result) {
                    var _historialArchivoProspecto = result;

                    if (_historialArchivoProspecto.type == "OK") {

                        swal({
                            position: 'top-right',
                            type: 'success',
                            title: _historialArchivoProspecto.message,
                            showConfirmButton: false,
                            timer: 2000
                        });
                    } else {
                        swal({
                            position: 'top-right',
                            type: 'error',
                            title: _historialArchivoProspecto.message,
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }

                    window.location.reload();
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] == "{")
                        err = JSON.parse(xhr.responseText).Message;
                    console.log(err);
                }

            })


        }
        else {
            swal({
                position: 'top-right',
                type: 'error',
                title: "Seleccione de la tabla los tickets que desea asignar, luego seleccione el usuario encargado",
                showConfirmButton: false,
                timer: 2000
            });
        }


    });

});


