﻿var fechaAgendamiento;

$(document).ready(function () {

    //lenguaje
    var initialLocaleCode = 'es';

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },

        locale: initialLocaleCode,
        buttonIcons: true, // show the prev/next text
        weekNumbers: false,
        editable: true,
        eventLimit: true, // allow "more" link when too many event
        timeZone: 'local',
        events: function (start, end, timezone, callback) {
            $.ajax({
                url: '/Programacion/AgendamientoCalendario',
                type: "GET",
                dataType: "JSON",

                success: function (result) {
                    var events = [];
                    var _programacionLLamadas = result.message;

                    //var formatearFecha = moment.utc(_programacionLLamadas.CreatedAt);
                    //var fecha = formatearFecha.format('YYYY-MM-DD');

                    $.each(_programacionLLamadas, function (i, data) {

                        events.push(
                            {
                                title: data.Descripcion,
                                description: data.Descripcion,
                                start: moment(data.Fecha_Inicio),
                                end: moment(data.Fecha_Fin),
                                backgroundColor: "#FE1717",
                                borderColor: "#fc0101"
                            }),

                        console.log("item pushed " + data);
                    });

                    callback(events);
                }
            });
        },

        dayClick: function (date, jsEvent, view) {
            //alert('Has hecho click en: ' + date.format());

            fechaAgendamiento = date.format();
            $(".agendarEvent").modal('show');

            $(".agendarEvent").modal({
                closable: true
            });

            //$('#cliente option:selected').text();
;
        },
        eventClick: function (calEvent, jsEvent, view) {
        },

        editable: false
    });
}); 




$("body").on("click", "#GuardarEventoAgendamiento", function () {

    var fechaProgramada = fechaAgendamiento + " " + $("#horaLlamadaAgendamiento").val() + ":" + $("#minutoLlamarAgendamiento").val();

    var data = {
        ClienteId: $('#cliente option:selected').val(),
        FECHA_INICIO: fechaProgramada,
        DESCRIPCION: 'llamada cliente'
    }


    //var data = {
    //    ProspectoId: $("#ProspectoId").val(),
    //    Nombre: $("#Nombre").val(),
    //    FechaOferta: $("#FechaOferta").val(),
    //    FechaLimiteOferta: $("#FechaLimiteOferta").val(),
    //    Descripcion: $("#descripcionOferta").val(),

    //}

    $.post("/Programacion/GuardarEventoCalendario", data)
        .done((Result) => {
            console.log(Result)
            swal({
                position: 'top-right',
                type: 'success',
                title: 'Evento agendado correctamente',
                showConfirmButton: false,
                timer: 1000
            });
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })

    $('#formularioAgendamiento').trigger("reset");

    $(".agendarEvent").modal('hide');

    location.reload();
})
