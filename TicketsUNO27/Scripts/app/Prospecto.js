﻿$(document).ready(function () {

    $.get("/OfertasProspectos/HistorialOfertasProspecto", { id: $("#ProspectoId").val() })
        .done((Result) => {
            var _oferta = Result.message;
            if (_oferta != null) {
                $("#Nombre").val(_oferta.Nombre);
                var fechaOferta = moment(_oferta.FechaOferta).format('YYYY-MM-DD h:mm:ss');
                var fechaLimiteOferta = moment(_oferta.FechaLimiteOferta).format('YYYY-MM-DD h:mm:ss');
                $('#FechaOferta').val(fechaOferta);
                $('#FechaLimiteOferta').val(fechaLimiteOferta);
                $('#descripcionOferta').val(_oferta.Descripcion);
                //mostrar boton modificar oferta actual
                $("#GuardarOfertaProspecto").hide();
                $("#EditarOfertaProspecto").show();
            }
            else {
                $("#GuardarOfertaProspecto").show();
                $("#EditarOfertaProspecto").hide();
            }
            
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })

});





$("body").on("click", "#GuardarOfertaProspecto", function () {

    var data = {
        ProspectoId: $("#ProspectoId").val(),
        Nombre: $("#Nombre").val(),
        FechaOferta: $("#FechaOferta").val(),
        FechaLimiteOferta: $("#FechaLimiteOferta").val(),
        Descripcion: $("#descripcionOferta").val(),

    }

    $.post("/OfertasProspectos/Create", data)
        .done((Result) => {
            console.log(Result)
            //if (Result.status == 201) {
                     
            HistorialOfertasProspecto();
            swal({
                position: 'top-right',
                type: 'success',
                title: 'La oferta se agrego correctamente',
                showConfirmButton: false,
                timer: 1000
            });
            $("#GuardarOfertaProspecto").hide();
            $("#EditarOfertaProspecto").show();
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })

      $('#formuOfertaProspecto').trigger("reset");
 })

function HistorialOfertasProspecto() {

    $.get("/OfertasProspectos/HistorialOfertasProspecto", { id: $("#ProspectoId").val() }  )
        .done((Result) => {
            console.log(Result);
            var _historialOfertaProspecto = Result.message;
            //agregar historial oferta a la tabla

            AgregarATabla(_historialOfertaProspecto, "#historialOfertas")

            $("#Nombre").val(_historialOfertaProspecto.Nombre);

            var fechaOferta = moment(_historialOfertaProspecto.FechaOferta).format('YYYY-MM-DD h:mm:ss');
            var fechaLimiteOferta = moment(_historialOfertaProspecto.FechaLimiteOferta).format('YYYY-MM-DD h:mm:ss');
            $('#FechaOferta').val(fechaOferta);
            $('#FechaLimiteOferta').val(fechaLimiteOferta);
            $('#descripcionOferta').val(_historialOfertaProspecto.Descripcion);


        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })


}


function AgregarATabla(Item, tbl) {


    if (tbl == "#historialOfertas") {
        const ElementItem =

            "<tr>" +
            "<td>" + Item.Prospecto.Nombre + "</td>" +
             "<td>" + Item.Nombre + "</td>" +
            "<td>" + Item.Descripcion + "</td>" +
            "<td>" + Item.ClienteId + "</td>" +
            "<td>" + moment(Item.FechaOferta).format('DD/MM/YYYY h:mm A') + "</td>" +
            "<td>" + moment(Item.FechaLimiteOferta).format('DD/MM/YYYY h:mm A') + "</td>" +  
            "<td>" + moment(Item.CreatedAt).format('DD/MM/YYYY h:mm A') + "</td></tr>"

        $(tbl).prepend(ElementItem);   
    }

    if (tbl == "#historialArchivo") {


        if (Item.TipoArchivo.Name == "Excel" || Item.TipoArchivo.Name == "word" || Item.TipoArchivo.Name == "pdf") {

            var boton = "<td><a id='descargarArchivo' value='Descargar' class='ui positive button' href='/Prospecto/DescargarArchivo/" + Item.Id + "'>Descargar</a></td>"
        }
        if (Item.TipoArchivo.Name == 'Imagen') {

            var boton =
            '<td><input type="button" class="ui primary button"  value="Detalle" onclick="DetalleOferta(\'' + Item.Archivo + '\');" >' +      
            "<a id='descargarArchivo' value='Descargar' class='ui positive button' href='/Prospecto/DescargarArchivo/" + Item.Id + "'>Descargar</a></td>"
        }
        if (Item.TipoArchivo.Name == 'Texto') {

            var boton =
                "<td><strong>" + Item.Archivo  + "</strong></td>";
        }

        const ElementItem =

            "<tr>" +
            "<td>" + Item.Nombre + "</td>" +
            "<td>" + Item.Descripcion + "</td>" +
            boton

       

        $(tbl).prepend(ElementItem);

    }

};

$("body").on("click", "#GuardarArchivosProspecto", function (e) {
    if ($('#fileUpload')[0].files[0] == undefined) {
        swal({
            position: 'top-right',
            type: 'error',
            title: 'Por favor seleccione el archivo',
            showConfirmButton: false,
            timer: 1200
        });
        return false;
    } else {
        data = new FormData();
        data.append('file', $('#fileUpload')[0].files[0]);
        data.append("prospectoId", $("#ProspectoId").val());
        data.append("descripcion", $("#descripcionArchivo").val());

        $.ajax({
            type: "POST",
            url: '/Archivos/CargarArchivo',
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                console.log(result);
                var _historialArchivoProspecto = result;

                if (_historialArchivoProspecto.type == "OK") {
                    ObtenerListaArchivos();
                    swal({
                        position: 'top-right',
                        type: 'success',
                        title: _historialArchivoProspecto.message,
                        showConfirmButton: false,
                        timer: 1000
                    });
                } else {
                    swal({
                        position: 'top-right',
                        type: 'error',
                        title: _historialArchivoProspecto.message,
                        showConfirmButton: false,
                        timer: 1000
                    });
                }

                $('#formCargarArchivo').trigger("reset");
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                console.log(err);
            }

        })
    }

   

});

function ObtenerListaArchivos() {

    $.get("/Archivos/HistorialArchivosProspecto", { id: $("#ProspectoId").val() })
        .done((Result) => {
            console.log(Result);
            var _historialArchivoProspecto = Result.message;
            //agregar historial archivo a la tabla

            AgregarATabla(_historialArchivoProspecto, "#historialArchivo")
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })


}


function DetalleOferta(id) {

    $("#detallearchivo").show();

    $('#detalleImagen').attr('src', 'data:image/jpeg;base64,' + id).show();
}


function CerrarDetalle() {

    $("#detallearchivo").hide();
}

function OfertaProspecto(id) {


    $.get("/OfertasProspectos/ObtenerOfertaProspecto", { id })
        .done((Result) => {
            console.log(Result);

            //reset dapickers


            var ofertaProspecto = Result.message;

            var formatearFechaOferta = moment.utc(ofertaProspecto.FechaOferta);
            var fechaOferta = formatearFechaOferta.format('YYYY-MM-DD');
            var formatearFechaLimiteOferta = moment.utc(ofertaProspecto.FechaLimiteOferta);
            var fechaLimiteOferta = formatearFechaLimiteOferta.format('YYYY-MM-DD'); 

            $('#NombreOfertaProspecto').val(ofertaProspecto.Nombre);
            $('#DescripcionOfertaProspecto').val(ofertaProspecto.Descripcion);

            //set in datepicker
            $("#FechaEdicionOferta").datepicker("setDate", fechaOferta);
            $("#FechaEdicionLimiteOferta").datepicker("setDate", fechaLimiteOferta);

            $(".test").modal('show');
            $(".test").modal({
            closable: true
            });

        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })

}

$("body").on("click", "#EditarOfertaProspecto", function (e) {
    var ofertasProspectos = {
        ProspectoId: $("#ProspectoId").val(),
        Descripcion: $('#descripcionOferta').val(),
        Nombre: $('#Nombre').val(),
        FechaOferta: $("#FechaOferta").val(),
        FechaLimiteOferta: $("#FechaLimiteOferta").val(),
    };

    $.ajax({
        url: "/OfertasProspectos/EditarOfertaProspecto",
        data: JSON.stringify(ofertasProspectos),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var _historialOfertaProspecto = result.message;
            AgregarATabla(_historialOfertaProspecto, "#historialOfertas")
            swal({
                position: 'top-right',
                type: 'success',
                title: 'La oferta se modifico correctamente',
                showConfirmButton: false,
                timer: 1000
            });
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
})
