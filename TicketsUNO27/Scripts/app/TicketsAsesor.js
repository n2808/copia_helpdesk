﻿//$(document).ready(function () { $.fn.dataTable.ext.errMode = "none"; $("#tblTicketsAsesor").DataTable({ ajax: { url: "/Tickets/ListarTicketsAsesor", type: "GET", datatype: "json" }, columns: [{ data: "Id", autoWidth: !0 }, { data: "Users.Nombres", autoWidth: !0 }, { data: "Estados.Nombre", autoWidth: !0 }, { data: "User.Documento", autoWidth: !0 }, { data: "User.Nombres", autoWidth: !0 }, { data: "User.Celular", autoWidth: !0 }, { data: "User.CodigoAcceso", autoWidth: !0 }, { data: "User.Campana.Nombre", autoWidth: !0 }, { data: "Area.Nombre", autoWidth: !0 }, { data: "TipoCaso.Nombre", autoWidth: !0 }, { data: "CreatedAt", autoWidth: !0, render: function (a, t, e) { return moment(a).format("YYYY-MM-DD h:mm:ss") } }, { data: "FechaFinalizacion", autoWidth: !0, render: function (a, t, e) { return moment(a).format("YYYY-MM-DD h:mm:ss") } }] }) });

$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    var tablaTicketsAsesor = $('#tblTicketsAsesor').DataTable({
        'ajax': {
            "url": "/Tickets/ListarTicketsAsesor",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "Id", "autoWidth": true },
            { "data": "Users.Nombres", "autoWidth": true },
            { "data": "Estados.Nombre", "autoWidth": true },
            { "data": "User.Documento", "autoWidth": true },
            { "data": "User.Nombres", "autoWidth": true },
            { "data": "User.Celular", "autoWidth": true },
            { "data": "User.Campana.Nombre", "autoWidth": true },
            { "data": "Area.Nombre", "autoWidth": true },
            { "data": "TipoCaso.Nombre", "autoWidth": true },
            { "data": "TipoCaso.Prioridad", "autoWidth": true },
            {
                "data": "CreatedAt", "autoWidth": true, render: function (data, type, row) {
                    return moment(data).format("YYYY-MM-DD h:mm:ss");
                }
            },
            {
                "data": "FechaFinalizacion", "autoWidth": true, render: function (data, type, row) {
                    if (data == null) {
                        return '';
                    } else {
                        return moment(data).format("YYYY-MM-DD h:mm:ss");
                    }                 
                }
            },
            {
                "title": "Actions",
                "data": "Id",
                "searchable": false,
                "sortable": false,

                "render": function (data, type, full, meta) {
                    return '<a  href="/DetalleAperturaTicket/Edit/' + data + '" class="ui green button">Detalle</a>';
                }
            }
        ]

    });

});
