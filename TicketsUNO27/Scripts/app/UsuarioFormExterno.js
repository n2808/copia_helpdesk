﻿$("body").on("click", "#GuardarUsuario", function () {

    var data = {
        Documento: $("#Documento").val(),
        Nombres: $("#Nombres").val(),
        Apellidos: $("#Apellidos").val(),
        Correo: $("#Correo").val(),
        Clave: $("#Clave").val(),
        Login: $("#Login").val(),
        TelefonoFijo: $("#TelefonoFijo").val(),
        Celular: $("#Celular").val(),
        CodigoAcceso: $("#CodigoAcceso").val(),
        CampanaId: $('#CampanaId option:selected').val()
    }

    $.post("/FormUsuario/Create", data)
        .done((Result) => {
            console.log(Result)
            if (Result.type == "OK") {
                swal({
                    position: 'top-right',
                    type: 'success',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
                setTimeout(function () { location.href = "/Home/HomeAdmin" }, 2000); 
            }

            if (Result.type == "ERROR") {
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }

         
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })

    $('#formCreacionUsuarioExterno').trigger("reset");
 })