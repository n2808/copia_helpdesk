﻿$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    var tablaEstadoTickets = $('#tblEstadoTickets').DataTable({
        'ajax': {
            "url": "/Tickets/ListarTickets",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "Id", "autoWidth": true },
            {
                "data": "Incidente",
                "autoWidth": true,
                "className": 'CellWithComment',
                "render": function (data) {
                    return '<span class="textoIncidenciaTamano" title="' + data + '">' + data + '</span>';
                }
            },
            {
                "data": "Respuesta",
                "autoWidth": true,
                "className": 'CellWithComment',
                "render": function (data) {
                    return '<span class="textoIncidenciaTamano" title="' + data + '">' + data + '</span>';
                }
            },
            { "data": "Users.Nombres", "autoWidth": true },
            { "data": "Estados.Nombre", "autoWidth": true },
            { "data": "User.Documento", "autoWidth": true },
            { "data": "User.Nombres", "autoWidth": true },
            { "data": "User.Celular", "autoWidth": true },
            { "data": "User.CodigoAcceso", "autoWidth": true },
            { "data": "User.Campana.Nombre", "autoWidth": true },
            { "data": "Area.Nombre", "autoWidth": true },
            { "data": "TipoCaso.Nombre", "autoWidth": true },
            {
                "data": "CreatedAt", "autoWidth": true, render: function (data, type, row) {
                    return moment(data).format("YYYY-MM-DD h:mm:ss");
                }
            },
            {
                "data": "FechaFinalizacion", "autoWidth": true, render: function (data, type, row) {

                    if (data == null) {
                        return '';
                    } else {
                        return moment(data).format("YYYY-MM-DD h:mm:ss");
                    }
                  
                }
            },
            {
                "title": "Actions",
                "data": "Id",
                "searchable": false,
                "sortable": false,

                "render": function (data, type, full, meta) {
                    return '<a  href="/DetalleEstadoTicket/ResumentTicket/' + data + '" class="ui green button">Detalle</a>';
                }
            }
        ]

    });

});

