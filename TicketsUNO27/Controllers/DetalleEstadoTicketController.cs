﻿using AppRecordatorio.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace TicketsUNO27.Controllers
{
    public class DetalleEstadoTicketController : Controller
    {
        private Contexto db = new Contexto();
        // GET: DetalleEstadoTicket
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ResumentTicket(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tickets = db.Tickets.Where(ts => ts.Id == id).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(u => u.User.Campana).Include(u => u.Users).FirstOrDefault();

            if (tickets == null)
            {
                return HttpNotFound();
            }
            ViewBag.Modalidad = tickets.Modalidad;
            ViewBag.IdAsignadoPor = tickets.IdAsignadoPor;
            ViewBag.IdTipoCaso = new SelectList(db.TipoCasos, "Id", "Nombre", tickets.IdTipoCaso);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Documento", tickets.IdUsuario);
            ViewBag.IdEstado = new SelectList(db.Estados.Where(e => e.Id == 8), "Id", "Nombre", tickets.IdEstado);
            ViewBag.HistorialTicket = db.HistorialTickets.Where(t => t.IdTicket == id).Include(e => e.Estados).Include(u => u.TicketAsignadoA).Include(u => u.TicketAsignadoPor).ToList();
            return View(tickets);
        }
    }
}