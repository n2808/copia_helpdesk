﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using tmkhogares.ViewModel;
using System.Diagnostics;
using Core.Models.User;
using Core.Models.Security;
using AppRecordatorio.Models;

namespace TicketsUNO27.Controllers
{
    public class HomeController : Controller
    {
        private Contexto  db = new Contexto();
        //private EstadosGestion estadosGestion = new EstadosGestion();

        public ActionResult Index()
        {
            return View();
        }


        [CustomAuthorize]
        public ActionResult HomeAdmin()
        {
            return View();
        }




  
        /*
         * DEBUG FUNTION.
         */
        private void DLog(string msg = "")
        {
            
            Debug.WriteLine("-------------------------------------------------------\n");
            Debug.WriteLine(msg + "\n");
            Debug.WriteLine("-------------------------------------------------------\n");
        }



    }
}