﻿using AppRecordatorio.Models;
using AppRecordatorio.Models.TicketsCasos;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace TicketsUNO27.Controllers
{
    public class DetalleAperturaTicketController : Controller
    {
        private Contexto db = new Contexto();
        // GET: DetalleTicketAsesor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Tickets tickets = db.Tickets.Find(id);

            var tickets = db.Tickets.Where(ts => ts.Id == id).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(u => u.User.Campana).Include(u => u.Users).FirstOrDefault();

            if (tickets == null)
            {
                return HttpNotFound();
            }
            ViewBag.Modalidad = tickets.Modalidad;
            ViewBag.IdAsignadoPor = tickets.IdAsignadoPor;
            ViewBag.IdTipoCaso = new SelectList(db.TipoCasos, "Id", "Nombre", tickets.IdTipoCaso);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Documento", tickets.IdUsuario);
            ViewBag.IdEstado = new SelectList(db.Estados.Where(e => e.Id == 8), "Id", "Nombre", tickets.IdEstado);
            //ViewBag.HistorialArchivos = db.Archivos.Where(a => a.IdRespuestaPor == tickets.IdAsignadoPor).ToList();

            ViewBag.HistorialArchivos = db.Archivos.Where(a => a.TicketId == id && a.IdRespuestaPor == tickets.IdAsignado).ToList();
            
            return View(tickets);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tickets tickets)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                    db.Entry(tickets).Entity.FechaFinalizacion = null;
                    db.Entry(tickets).State = EntityState.Modified;
                    db.SaveChanges();

                    //return RedirectToAction("MisTickets");
                }
                ViewBag.IdTipoCaso = new SelectList(db.TipoCasos, "Id", "Nombre", tickets.IdTipoCaso);
                ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Documento", tickets.IdUsuario);


                return View("~/Views/Tickets/MisTickets.cshtml");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "" + "trace" + ex.StackTrace);
            }
        }
    }
}