﻿using AppRecordatorio.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using OfficeOpenXml;
using TicketsUNO27.helper;
using Core.Models.User;
using TicketsUNO27.Models.Constants;

namespace TicketsUNO27.Controllers
{
    public class CargueUsuariosController : Controller
    {
        private Contexto db = new Contexto();
        // GET: CargueUsuarios
        public ActionResult Index()
        {
            return View(db.Campanas.ToList());
        }

        public JsonResult CargarUsuarios(int idCampana)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    var dtUsuarios = LeerArchivoExcel(file.InputStream);

                    if (dtUsuarios.Rows.Count > 0)
                    {
                        if (InsertarUsuarios(dtUsuarios, idCampana) == true)
                        {
                            return Json(new { status = 201, message = "Cargue masivo exitoso." });

                        }
                        else
                        {
                            return Json(new { status = 504, message = "Revise la información, algunos datos son obligatorios." });

                        }
                    }
                }

                return Json(new { status = 504, message = "Seleccione el archivo" });



            }
            catch (Exception e)
            {
                return Json(new { status = 504, message = "Error consulte con el administrador" + e.Message + "" });
            }

        }


        public DataTable LeerArchivoExcel(Stream stream)
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            var excel = new ExcelPackage(stream);

            var dt = excel.ToDataTableUsuario();

            return dt;
        }


        public bool InsertarUsuarios(DataTable dt, int idCampana)
        {
            if (idCampana <= 0)
            {
                return false;
            }


            using (var context = new Contexto())
            {
                // CREATING A BEGIN TRANSACTION AND COMMIT.
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //BEGIN OF THE QUERY
                        foreach (DataRow row in dt.Rows)
                        {
                            User usuario = new User();
                            usuario.Documento = row["DOCUMENTO"].ToString().Trim();
                            usuario.Nombres = row["NOMBRES"].ToString().Trim();
                            usuario.CampanaId = idCampana;
                            usuario.Estado = true;
                            usuario.Login = row["DOCUMENTO"].ToString().Trim();
                            usuario.Clave = row["DOCUMENTO"].ToString().Trim();
                            usuario.CreatedAt = DateTime.Now;
                            usuario.Celular = row["TELEFONO"].ToString().Trim();

                            db.Usuario.Add(usuario);
                            db.SaveChanges();

                            InsertarRol(usuario.Id);
                        }


                        //COMMIT TRANSACTION
                        dbContextTransaction.Commit();
                        return true;


                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);

                    }
                }
            }

        }


        public void InsertarRol(int idUsuario)
        {
            try
            {
                UserRol userRol = new UserRol();

                userRol.RolId = Constants.RolAsesor;
                userRol.UserId = idUsuario;
                userRol.CreatedAt = DateTime.Now;

                db.UsuariosRoles.Add(userRol);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}