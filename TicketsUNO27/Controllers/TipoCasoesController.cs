﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppRecordatorio.Models;
using AppRecordatorio.Models.TipoCasos;
using Core.Models.Security;

namespace TicketsUNO27.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class TipoCasoesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: TipoCasoes
        public ActionResult Index()
        {
            var tipoCasos = db.TipoCasos.Include(t => t.Area);
            return View(tipoCasos.ToList());
            //return View();
        }

        // GET: TipoCasoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCaso tipoCaso = db.TipoCasos.Find(id);
            if (tipoCaso == null)
            {
                return HttpNotFound();
            }
            return View(tipoCaso);
        }

        // GET: TipoCasoes/Create
        public ActionResult Create()
        {
            ViewBag.IdArea = new SelectList(db.Areas, "Id", "Nombre");
            return View();
        }

        // POST: TipoCasoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TipoCaso tipoCaso)
        {
            if (ModelState.IsValid)
            {
                db.TipoCasos.Add(tipoCaso);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.IdArea = new SelectList(db.Areas, "Id", "Nombre", tipoCaso.IdArea);
            return View(tipoCaso);
        }

        // GET: TipoCasoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCaso tipoCaso = db.TipoCasos.Find(id);
            if (tipoCaso == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdArea = new SelectList(db.Areas, "Id", "Nombre", tipoCaso.IdArea);
            ViewBag.Estado = new SelectList(db.Areas, "Id", "Nombre", tipoCaso.IdArea);
            return View(tipoCaso);
        }

        // POST: TipoCasoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdArea,Nombre,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy,Prioridad")] TipoCaso tipoCaso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoCaso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.IdArea = new SelectList(db.Areas, "Id", "Nombre", tipoCaso.IdArea);
            return View(tipoCaso);
        }

        // GET: TipoCasoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCaso tipoCaso = db.TipoCasos.Find(id);
            if (tipoCaso == null)
            {
                return HttpNotFound();
            }
            return View(tipoCaso);
        }

        // POST: TipoCasoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoCaso tipoCaso = db.TipoCasos.Find(id);
            db.TipoCasos.Remove(tipoCaso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
