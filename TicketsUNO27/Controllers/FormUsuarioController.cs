﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppRecordatorio.Models;
using Core.Models.User;

namespace TicketsUNO27.Controllers
{
    public class FormUsuarioController : Controller
    {
        private Contexto db = new Contexto();

        // GET: FormUsuario
        public ActionResult Index()
        {
            var usuario = db.Usuario.Include(u => u.Campana);
            return View(usuario.ToList());
        }

        // GET: FormUsuario/Create
        public ActionResult Create()
        {
            ViewBag.CampanaId = new SelectList(db.Campanas, "Id", "Nombre");
            return View();
        }

        // POST: FormUsuario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,Documento,Nombres,Apellidos,Correo,Estado,Clave,Login,TelefonoFijo,Celular,CodigoAcceso,CampanaId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            try
            {

                //VALIDAR SI USUARIO YA EXISTE
                var usuario = db.Usuario.Where(d => d.Documento == user.Documento).ToList().Count();

                if (usuario == 1)
                {
                    return Json(new { type = "ERROR", message = "El usuario ya se encuentra registrado consulte con el administrador." }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    if (ModelState.IsValid)
                    {

                        db.Entry(user).Entity.Estado = true;

                        db.Usuario.Add(user);

                        db.SaveChanges();

                        //id insertado
                        int id = user.Id;

                        //crear rol usuario asesor
                        UserRol rol = new UserRol();

                        rol.RolId = 4;
                        rol.UserId = id;
                        db.UsuariosRoles.Add(rol);
                        db.SaveChanges();
                    }
                }


            }
            catch (Exception ex)
            {
                return Json(new { type = "ERROR", message = "Error al registrar usuario consulte con el administrador." }, JsonRequestBehavior.DenyGet);
            }

            ViewBag.CampanaId = new SelectList(db.Campanas, "Id", "Nombre", user.CampanaId);
            return Json(new { type = "OK", message = "Usuario Registrado Correctamente." }, JsonRequestBehavior.DenyGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
