﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppRecordatorio.Models;
using AppRecordatorio.Models.Campanas;
using Core.Models.Security;

namespace TicketsUNO27.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class CampanasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Campanas
        public ActionResult Index()
        {
            return View(db.Campanas.ToList());
        }

        // GET: Campanas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // GET: Campanas/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Campanas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {
                db.Campanas.Add(campana);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(campana);
        }

        // GET: Campanas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // POST: Campanas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campana).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(campana);
        }

        // GET: Campanas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campanas.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // POST: Campanas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Campana campana = db.Campanas.Find(id);
            db.Campanas.Remove(campana);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
