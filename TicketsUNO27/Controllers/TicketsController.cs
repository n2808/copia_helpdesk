﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppRecordatorio.Models;
using AppRecordatorio.Models.TicketsCasos;
using Core.Models.Security;
using PagedList;
using TicketsUNO27.Models.Archivos;
using TicketsUNO27.Models.Constants;
using TicketsUNO27.Models.Historial;
using TicketsUNO27.ViewModel;

namespace TicketsUNO27.Controllers
{
    public class TicketsController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Tickets
        public ActionResult Index()
        {


            var usuario = SessionPersister.Id;

            var rol = db.UsuariosRoles.Where(ur => ur.UserId == usuario).Select(r => r.Rol.Name).FirstOrDefault();

            if (rol == "Soporte Asignacion")
            {
                ViewBag.Usuarios = new SelectList(db.UsuariosRoles.Where(ur => ur.UserId == usuario).Include(u => u.User).Select(c => new { c.UserId, Name = c.User.Nombres + "(" + c.User.Apellidos + ")" }), "UserId", "Name");

            }
            if (rol == "Lider Soporte" || rol == "Administrador")
            {
                ViewBag.Usuarios = new SelectList(db.UsuariosRoles.Where(ur => ur.RolId == 2 || ur.RolId == 3 || ur.RolId == 6).Include(u => u.User).Select(c => new { c.UserId, Name = c.User.Nombres + "(" + c.User.Apellidos + ")" }), "UserId", "Name");
            }

            ViewBag.SinAsignar = db.Tickets.Where(d => d.Estados.Nombre == "Nuevo").Count();
            ViewBag.EnProgreso = db.Tickets.Where(d => d.IdEstado == 3).Count();
            ViewBag.Resueltos = db.Tickets.Where(d => d.IdEstado == 4).Count();
            ViewBag.PrioridadAlta = Constants.PrioridadAlta;
            ViewBag.PrioridadMedia = Constants.PrioridadMedia;
            ViewBag.PrioridadBaja = Constants.PrioridadBaja;

            return View();
        }

        public ActionResult VerTicketsSoporte()
        {
            return View();
        }

        public ActionResult CargarTickets()
        {

            var usuario = SessionPersister.Id;
            List<Tickets> data = null;

            var rol = db.UsuariosRoles.Where(ur => ur.UserId == usuario).Select(r => r.Rol).FirstOrDefault();

            if (rol.Name == "Lider Soporte")
            {
                data = db.Tickets.Where(t => (t.IdAsignado == 853 || t.IdAsignado == usuario || t.IdEstado == 2 || t.IdEstado == 3) && t.IdEstado != 4 && t.IdEstado != 5 && t.IdEstado != 6 && t.IdArea != 2).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(u => u.Users).ToList();
            }
            if (rol.Name == "Soporte Asignacion")
            {
                data = db.Tickets.Where(t => (t.IdAsignado == 853 || t.IdAsignado == usuario || t.IdEstado == 2 || t.IdEstado == 3) && t.IdEstado != 4 && t.IdEstado != 5 && t.IdEstado != 6 && t.IdArea != 2).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(u => u.Users).ToList();
            }
            if (rol.Name == "Soporte")
            {
                data = db.Tickets.Where(ts => ts.Area.Nombre == rol.Name && ts.IdAsignado == usuario && ts.IdEstado != 4 && ts.IdEstado != 5 && ts.IdEstado != 6 && ts.IdArea != 2).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(u => u.Users).ToList();
            }

            if (rol.Name == "Recursos Humanos")
            {
                data = db.Tickets.Where(ts => ts.Area.Nombre == rol.Name && ts.IdAsignado == usuario && ts.IdEstado != 4 && ts.IdEstado != 5 && ts.IdEstado != 6).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(u => u.Users).ToList();
            }


            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListaEstadoTickets(int? page, string ticket, string estado)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["pagination"]);

            int pageNumber = (page ?? 1);


            var data = db.Tickets
                 .Where(t => t.Id.ToString().StartsWith(ticket.Trim()) || ticket == null || ticket == "")
                 .Where(t => t.IdEstado.ToString().StartsWith(estado.Trim()) || estado == null || estado == "")
                .Include(d => d.Estados)
                .Include(t => t.TipoCaso)
                .Include(e => e.Estados)
                .Include(a => a.Area)
                .Include(u => u.User)
                .Include(c => c.User.Campana).Include(a => a.Users)
                .OrderByDescending(c => c.CreatedAt)
                .ToPagedList(pageNumber, pageSize);


            ViewBag.SinAsignar = db.Tickets.Where(d => d.Estados.Nombre == "Nuevo").Count();
            ViewBag.EnProgreso = db.Tickets.Where(d => d.IdEstado == 3).Count();
            ViewBag.Resueltos = db.Tickets.Where(d => d.IdEstado == 4).Count();
            return View(data);

        }

        public ActionResult GestionTickets()
        {
            return View();
        }

        public ActionResult ListaGestionTickets()
        {
            var usuario = SessionPersister.Id;

            var data = db.Tickets.Where(t => t.IdAsignado == usuario && t.Estados.EsFinal == true).Include(d => d.Estados).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(a => a.Users).ToList();

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MisTickets()
        {
            ViewBag.PrioridadAlta = Constants.PrioridadAlta;
            ViewBag.PrioridadMedia = Constants.PrioridadMedia;
            ViewBag.PrioridadBaja = Constants.PrioridadBaja;
            return View();
        }

        public ActionResult ListarTicketsAsesor()
        {
            var usuario = SessionPersister.Id;
            List<Tickets> data = null;

            data = db.Tickets.Where(t => t.IdUsuario == usuario).Include(d => d.Estados).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(c => c.User.Campana).Include(a => a.Users).ToList();


            //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //serializer.MaxJsonLength = 500000000;

            var json = Json(new { data = data }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = 500000000;

            return json;
        }


        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tickets tickets = db.Tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // GET: Tickets/Create
        public ActionResult Create(string id)
        {

            ViewBag.Documento = db.Usuario.Where(u => u.Documento == id).Select(s => s.Documento).FirstOrDefault();
            ViewBag.IdUsuario = db.Usuario.Where(u => u.Documento == id && u.Estado == true).Select(s => s.Id).FirstOrDefault();
            ViewBag.IdArea = new SelectList(db.Areas, "Id", "Nombre");

            return View();
        }


        public JsonResult AsignarTicketUsuario(usuarioSeleccion usuarioSeleccion)
        {
            var usuarioActual = SessionPersister.Id;

            foreach (var seleccionado in usuarioSeleccion.Id)
            {

                var actual = db.Tickets.Where(t => t.Id == seleccionado).FirstOrDefault();

                db.Entry(actual).State = EntityState.Modified;
                db.Entry(actual).Entity.IdAsignado = Convert.ToInt32(usuarioSeleccion.IdAsignado);
                db.Entry(actual).Entity.IdAsignadoPor = usuarioActual;
                db.Entry(actual).Entity.ActualizadoPor = SessionPersister.Id;
                db.Entry(actual).Entity.Modalidad = actual.Modalidad;
                db.Entry(actual).Entity.IdEstado = 2;
                //fecha desde la creación hasta asignado
                db.Entry(actual).Entity.UpdatedAt = DateTime.Now;
                db.SaveChanges();

                HistorialTickets(actual);

            }
            return Json(new { type = "OK", message = "El ticket se asigno correctamente al usuario " }, JsonRequestBehavior.DenyGet);
        }


        public JsonResult CargarEvidenciaCaso(string documento, string area, string idTipoCaso)
        {

            try
            {

                if (Request.Files.Count > 0)
                {
                    var test = Request.Files[0];

                    string nombreArchivo = "Gestion_" + documento + "_TIPO_";

                    nombreArchivo = nombreArchivo + idTipoCaso + "_AREA_" + area;
                    nombreArchivo = nombreArchivo + ".jpg";

                    string nombre_archivo = @"\\172.16.5.132\Repositorio_Aplicaciones\imagenes_helpDesk\" + nombreArchivo;

                    test.SaveAs(nombre_archivo);

                }

            }
            catch (Exception ex)
            {
                return Json(new { type = "ERROR", message = "La evidencia no se pudo cargar " }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { type = "OK", message = "La evidencia se cargo correctamente" }, JsonRequestBehavior.DenyGet);
        }


        public JsonResult ObtenerTipoCaso(int? IdArea)
        {

            var tiposCaso = db.TipoCasos.Where(t => t.IdArea == IdArea && t.Estado == true).OrderBy(c => c.Nombre).ToList();

            return Json(tiposCaso, JsonRequestBehavior.AllowGet);



        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create([Bind(Include = "IdUsuario, Modalidad ,IdTipoCaso,FechaFinalizacion,Estado,Incidente,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy, IdArea")] Tickets tickets)
        {
            int numeroTicket = 0;

            try
            {
                if (ModelState.IsValid)
                {
                    //estados iniciales
                    //obtener ip
                    if (Request.Files.Count > 0)
                    {
                        if (FormatosArchivoPermitidos())
                        {
                            db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                            db.Entry(tickets).Entity.CreadoPor = SessionPersister.Id;
                            db.Entry(tickets).Entity.IdAsignado = tickets.IdArea == 1 ? 853 : 874;
                            db.Entry(tickets).Entity.IdEstado = tickets.IdArea == 2 ? 2 : 1;
                            db.Tickets.Add(tickets);
                            db.SaveChanges();

                            numeroTicket = tickets.Id;
                            //con id ticket guardar imagen en compartida
                            CargarArchivoControlInventario(numeroTicket);

                        }
                        else
                        {
                            return Json(new { type = "ERROR", message = "El formato del archivo no es valido." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (Request.Files.Count == 0)
                    {
                        db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                        db.Entry(tickets).Entity.CreadoPor = SessionPersister.Id;
                        db.Entry(tickets).Entity.IdAsignado = tickets.IdArea == 1 ? 853 : 874;
                        db.Entry(tickets).Entity.IdEstado = tickets.IdArea == 2 ? 2 : 1;

                        db.Tickets.Add(tickets);
                        db.SaveChanges();

                        numeroTicket = tickets.Id;
                    }



                }


            }
            catch (Exception ex)
            {
                return Json(new { type = "ERROR", message = "Error al crear el ticket consulte con el administrador." }, JsonRequestBehavior.DenyGet);
            }
            return Json(new { type = "OK", message = "El Ticket " + numeroTicket + " se ha Creado Correctamente." }, JsonRequestBehavior.DenyGet);

        }

        public void CargarArchivoControlInventario(int numeroTicket)
        {
            var ticket = db.Tickets.Where(t => t.Id == numeroTicket).Include(a => a.Area).Include(t => t.TipoCaso).Include(u => u.User).FirstOrDefault();
            string bytesArchivo = "";
            if (Request.Files.Count > 0)
            {
                Archivos archivos = new Archivos();
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    var ext = Path.GetExtension(files[i].FileName).ToLower();
                    archivos.TicketId = ticket.Id;
                    archivos.Nombre = files[i].FileName;
                    archivos.ExtensionArchivo = ext;

                    //leer bytes
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(files[i].InputStream))
                    {
                        bytes = br.ReadBytes(files[i].ContentLength);
                    }

                    bytesArchivo = Convert.ToBase64String(bytes);
                    archivos.Archivo = bytesArchivo;
                    archivos.IdUsuarioTicket = ticket.User.Id;
                    db.Archivos.Add(archivos);
                    db.SaveChanges();
                }

            }

        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var tickets = db.Tickets.Where(ts => ts.Id == id).Include(t => t.TipoCaso).Include(e => e.Estados).Include(a => a.Area).Include(u => u.User).Include(u => u.User.Campana).FirstOrDefault();
            var estadoTiempoGestionTicket = false;

            if (tickets == null) return HttpNotFound();

            ViewBag.Modalidad = tickets.Modalidad;
            ViewBag.IdAsignadoPor = tickets.IdAsignadoPor;
            ViewBag.IdTipoCaso = new SelectList(db.TipoCasos, "Id", "Nombre", tickets.IdTipoCaso);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Documento", tickets.IdUsuario);
            ViewBag.IdEstado = new SelectList(db.Estados.OrderBy(x => x.Nombre).Where(x => x.Id <= 6), "Id", "Nombre", tickets.IdEstado);
            ViewBag.IdTipoAtencion = new SelectList(db.Estados.OrderBy(x => x.Nombre).Where((x => x.Id == Constants.AtencionRemota || x.Id == Constants.AtencionSitio)), "Id", "Nombre", tickets.IdEstado);

            ViewBag.HistorialArchivos = db.Archivos.Where(a => a.TicketId == id).ToList();

            ViewBag.Prioridad = tickets.TipoCaso.Prioridad;


            ViewBag.TiempoResolucion =
                tickets.TipoCaso.Prioridad == Constants.Alto
                ? "0-60 Minutos" : tickets.TipoCaso.Prioridad == Constants.Media
                ? "1-24 Horas" : "3-72 Horas";

            ViewBag.TiempoTranscurrido = CalcularTiempos(tickets.CreatedAt);

            if (CalcularEstadoTiempo(tickets.CreatedAt, tickets.TipoCaso.Prioridad))
            {
                estadoTiempoGestionTicket = true;
                TempData["Mensaje"] = "Has agotado el tiempo para la resolución del ticket, por favor especifíquelo en el campo Notas por demora en la gestión del ticket.";
            }

            ViewBag.estadoTiempoGestionTicket = estadoTiempoGestionTicket;

            return View(tickets);
        }


        public string CalcularTiempos(DateTime? fechaAsignado)
        {
            var difference = (DateTime.Now - (fechaAsignado ?? DateTime.Today));

            return String.Format("{0} days, {1} hours, {2} minutes, {3} seconds",
            difference.Days, difference.Hours, difference.Minutes, difference.Seconds);
        }

        public bool CalcularEstadoTiempo(DateTime? fechaAsignado, string prioridad)
        {            
       
            var estado      = false; 
            TimeSpan result = DateTime.Now.Subtract((DateTime)fechaAsignado);          

            switch (prioridad)
            {
                case Constants.Alto:

                    if (result.TotalHours > 1) estado = true;
                    break;

                case Constants.Media:

                    if (result.TotalHours > 24) estado = true;
                    break;

                default:
                    if (result.TotalHours > 72) estado = true;
                    break;
            }

            return estado;

        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdAsignado,Modalidad,IdAsignadoPor,IdUsuario,IdArea,IdTipoCaso,FechaFinalizacion,Estado,Incidente,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy,Respuesta,IdEstado,IdTipoAtencion,Notas,DetalleDemoraGestion")] Tickets tickets)
        {

            HttpFileCollectionBase files = Request.Files;

            if (tickets.IdEstado == 8)
            {
                return RedirectToAction("Index");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var archivos = db.Archivos.Where(a => a.TicketId == tickets.Id).ToList();

                    if (archivos.Count == 0)
                    {
                        db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                        db.Entry(tickets).Entity.FechaFinalizacion = DateTime.Now;
                        db.Entry(tickets).State = EntityState.Modified;
                        db.SaveChanges();

                        HistorialTickets(tickets);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        if (Request.Files.Count > 0)
                        {
                            var contenidoArchivo = files[0].ContentLength;
                            if (contenidoArchivo == 0)
                            {
                                db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                                db.Entry(tickets).Entity.FechaFinalizacion = DateTime.Now;
                                db.Entry(tickets).State = EntityState.Modified;
                                db.SaveChanges();
                                HistorialTickets(tickets);
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                if (FormatosArchivoPermitidos())
                                {
                                    db.Entry(tickets).Entity.IpUsuario = Request.ServerVariables["REMOTE_ADDR"];
                                    db.Entry(tickets).Entity.FechaFinalizacion = DateTime.Now;
                                    db.Entry(tickets).State = EntityState.Modified;
                                    db.SaveChanges();

                                    CargarRespuestaTicket(tickets);
                                    HistorialTickets(tickets);
                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    ViewBag.formatoError = "Error al actualizar el estado del ticket, verifique el formato del archivo.";
                                }
                            }

                        }
                    }

                }
            }


            ViewBag.IdTipoCaso = new SelectList(db.TipoCasos, "Id", "Nombre", tickets.IdTipoCaso);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Documento", tickets.IdUsuario);
            ViewBag.IdEstado = new SelectList(db.Estados, "Id", "Nombre", tickets.IdEstado);


            return View(tickets);
        }

        public void CargarRespuestaTicket(Tickets ticket)
        {
            string bytesArchivo = "";
            if (Request.Files.Count > 0)
            {
                Archivos archivos = new Archivos();
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    var ext = Path.GetExtension(files[i].FileName).ToLower();
                    archivos.TicketId = ticket.Id;
                    archivos.Nombre = files[i].FileName;
                    archivos.ExtensionArchivo = ext;

                    //leer bytes
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(files[i].InputStream))
                    {
                        bytes = br.ReadBytes(files[i].ContentLength);
                    }

                    bytesArchivo = Convert.ToBase64String(bytes);
                    archivos.Archivo = bytesArchivo;
                    archivos.IdRespuestaPor = ticket.IdAsignado;
                    db.Archivos.Add(archivos);
                    db.SaveChanges();
                }

            }
        }
        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tickets tickets = db.Tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tickets tickets = db.Tickets.Find(id);
            db.Tickets.Remove(tickets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public void HistorialTickets(Tickets actual)
        {
            //guardar historial ticket
            HistorialTickets historialTickets = new HistorialTickets();
            historialTickets.IdTicket = actual.Id;
            historialTickets.FechaCambioEstado = DateTime.Now;
            historialTickets.IdEstado = actual.IdEstado;
            historialTickets.Respuesta = actual.Respuesta;
            historialTickets.AsignadoPor = actual.IdAsignadoPor;
            historialTickets.AsignadoA = actual.IdAsignado;
            historialTickets.NotasTicket = actual.Notas;
            db.HistorialTickets.Add(historialTickets);
            db.SaveChanges();
        }

        public bool FormatosArchivoPermitidos()
        {
            bool formatoValido = false;

            string[] formatosSoportados = ConfigurationManager.AppSettings["FormatosArchivoValidos"].Split(';');
            HttpFileCollectionBase files = Request.Files;
            foreach (string x in formatosSoportados)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    if (file.FileName.ToLower().Contains(x))
                    {
                        return true;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return formatoValido;
        }
    }
}
