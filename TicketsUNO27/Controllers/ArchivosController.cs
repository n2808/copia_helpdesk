﻿using AppRecordatorio.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketsUNO27.Controllers
{
    public class ArchivosController : Controller
    {

        private Contexto db = new Contexto();
        // GET: Archivos
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DescargarArchivo(int id)
        {
            var base64 = db.Archivos.Find(id);

            byte[] archivoBytes = Convert.FromBase64String(base64.Archivo.ToString());
            MemoryStream memoryStreamArchivo = new MemoryStream();

            using (MemoryStream contentStream = new MemoryStream())
            {
                memoryStreamArchivo.Write(archivoBytes, 0, archivoBytes.Length);
            }

            byte[] bytesInStream = memoryStreamArchivo.ToArray();
            memoryStreamArchivo.Close();

            Response.Clear();
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition", "attachment;    filename=" + base64.Nombre);
            Response.BinaryWrite(bytesInStream);
            Response.End();

            return Json(new { status = 201, message = "el archivo se descargo correctamente" }, JsonRequestBehavior.AllowGet);

        }
    }
}