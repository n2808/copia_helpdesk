﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppRecordatorio.Models;
using Core.Models.Security;
using Core.Models.User;


namespace TicketsUNO27.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class UsersController : Controller
    {
        private Contexto db = new Contexto();

        /*------------------------------------------------------*
        * ADMINISTRACION DE USUARIOS
        *------------------------------------------------------*/

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Usuario.Where(c => c.Estado == true).ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.CampanaId = new SelectList(db.Campanas, "Id", "Nombre");

            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Documento,Nombres,Apellidos,Correo,Estado,Clave,Login,TelefonoFijo,Ceular,CodigoAcceso,CampanaId,UpdatedAt,DeletedAt")] User user)
        {
            ViewBag.CampanaId = new SelectList(db.Campanas, "Id", "Nombre");

            if (ModelState.IsValid)
            {
                //user.Id = Guid.NewGuid();
                db.Entry(user).Entity.Estado = true;
                db.Usuario.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.CampanaId = new SelectList(db.Campanas, "Id", "Nombre", user.CampanaId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Documento,Nombres,Apellidos,Correo,Estado,Clave,Login,TelefonoFijo,Celular,CodigoAcceso,CampanaId,UpdatedAt,DeletedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CampanaId = new SelectList(db.Campanas.Where(c => c.Id == user.CampanaId), "Id", "Nombre", user.CampanaId);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Usuario.Find(id);
            db.Usuario.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*------------------------------------------------------*
        * ADMINISTRACION DE ROLES
        *------------------------------------------------------*/
        
        public ActionResult Roles(int Id)
        {
            User Usuario = db.Usuario.Find(Id);
            if (Id == null || Usuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = Usuario.Nombres;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles([Bind(Include = "Id,RolId,UserId")] UserRol userRol, string Name)
        {
            if (ModelState.IsValid)
            {
                var Exist = db.UsuariosRoles
                    .Where(
                    c => c.UserId == userRol.UserId&&
                    c.RolId == userRol.RolId
                ).FirstOrDefault();

                if (Exist != null)
                {
                    ModelState.AddModelError("RolId", "este permiso ya fue asignado");
                }
                else
                {
                    //userRol.Id = Guid.NewGuid();
                    db.UsuariosRoles.Add(userRol);
                    db.SaveChanges();
                    return RedirectToAction("Roles", new { Id = userRol.UserId});
                }


            }
            var Id = userRol.UserId;
            ViewBag.Name = Name;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View("Roles");
        }

        // POST: UserOfPointOfCares/Delete/5
        [HttpPost, ActionName("DeleteRol")]
        [ValidateAntiForgeryToken]
        public ActionResult RolesDeleteConfirmed(int id)
        {
            UserRol UsusarioRol = db.UsuariosRoles.Find(id);
            var UserId = UsusarioRol.UserId;
            db.UsuariosRoles.Remove(UsusarioRol);
            db.SaveChanges();
            return RedirectToAction("Roles", new { Id = UserId });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
